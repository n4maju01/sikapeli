/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sikagui;

/**
 *
 * @author jjuntune
 */
public class Pelaaja {
    protected Noppa noppa;
    protected int pisteet=0;
    protected int vuoronPisteet=0;
    
    public Pelaaja() {
        noppa=new Noppa();
    }
    
    public Noppa getNoppa() {
        return this.noppa;
    }
    
    public int getPisteet() {
        return this.pisteet;
    }    
    
    public int getVuoronPisteet() {
        return this.vuoronPisteet;
    }
    
    public final boolean heita() {
        int silmaluku=0;
        this.noppa.heita();
        silmaluku=this.noppa.getSilmaluku();
        if (silmaluku!=1) {
            this.vuoronPisteet+=silmaluku;
            return true;
        }
        else {
            this.vuoronPisteet=0;
            return false;
        }
    }    
    
    public final void lopetaVuoro() {
        this.pisteet+=this.vuoronPisteet;
        this.vuoronPisteet=0;
    }
}
