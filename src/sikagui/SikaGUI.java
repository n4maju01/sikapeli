/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sikagui;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author Pekka
 */
public class SikaGUI extends Application {
    
    // Jäsenvakiot
    private final int LEVEYS_PIKSELIT = 860;
    private final int KORKEUS_PIKSELIT = 240;
    private final String SOVELLUKSEN_NIMI = "SikaGUI";
    private final String TYYLITIEDOSTON_POLKU = "style/style.css";
    private final String KUVATIEDOSTOJEN_HAKEMISTO = "images/";
    private final String KUVATIEDOSTOJEN_TARKENNE = ".png";
    private final String SIKA_KUVATIEDOSTO = "Sika";
    private final String SMILEY_KUVATIEDOSTO = "Smiley";
    private final String SAD_SMILEY_KUVATIEDOSTO = "Sad-smiley";
    private final String HEITA_NOPPAA = "Heitä noppaa";
    private final String KONEEN_VUORO = "Koneen vuoro";
    
    // Jäsenmuuttujat
    private Label lblPelaajanPisteet;
    private Label lblTietokoneenPisteet;
    private Label lblViesti;
    private Button btnHeitaNoppaa;
    private Button btnTietokoneenVuoro;
    private Image imgKuva;
    private ImageView imgvKuvaOhjain;
    private String pelaajanVuoronHeitot = new String();
    
    // Muiden luokkien olioista tulee tämän luokan jäsenolioita
    private Noppa noppa;
    private Pelaaja pelaaja;
    private Peli peli;
    private Tietokone tietokone;
    
    @Override
    public void start(Stage primaryStage) {
        
        // Luodaan ruudukko (grid) GridPane-luokasta
        GridPane grid = new GridPane();
        
        // Kutsutaan metodeja, jotka luovat ruudukko-tyyppisen käyttöliittymän
        luoSarakkeet(grid);
        luoRivit(grid);
        luoOhjaimet(grid);
        
        // Luodaan sovellukselle ikkuna välittämällä parametrina Scene-luokan
        // alustajalle ruudukko sekä ikkunan leveys ja korkeus pikseleinä
        Scene scene = new Scene(grid, LEVEYS_PIKSELIT, KORKEUS_PIKSELIT );
        
        // Otetaan tyylitiedosto käyttöön
        scene.getStylesheets().add(SikaGUI.class.getResource(
                TYYLITIEDOSTON_POLKU).toExternalForm());
                
        // Asetetaan sovelluksen otsikkopalkin teksti
        primaryStage.setTitle(SOVELLUKSEN_NIMI);
        
        // Liitetään Scene-luokasta luotu ikkunaolio Stage-luokasta luotuun
        // säiliöolioon
        primaryStage.setScene(scene);
        
        // Esitetään käyttöliittymä
        primaryStage.show();
        
        // Alustetaan peli
        alustaPeli();
        
        // Heitä noppaa -komentopainikkeen tapahtumankäsittelijä
        btnHeitaNoppaa.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                
                // Jos peli ei jatku, se on päättynyt. Alustetaan uusi peli.
                if (!(peli.getTilanne() == Peli.Pelitilanne.PELI_JATKUU)) {
                    alustaPeli();
                }
                
                // Heitetään noppaa. Jos tulee ykkönen, vuoro siirtyy
                // tietokoneelle. Disabloidaan komentopainike, jolla heitetään
                // noppaa
                if (!peli.getPelaaja().heita()) {
                    btnHeitaNoppaa.setDisable(true);
                }
                
                // Asetetaan nopan silmäluvun kuva käyttöliittymään
                asetaNopanKuva(peli.getPelaaja().getNoppa().getSilmaluku());
                
                // Näytetään pelaajan heittämät silmäluvut menossa olevalla 
                // vuorolla käyttöliittymän alalaidassa
                naytaPelaajanHeitot(
                        peli.getPelaaja().getNoppa().getSilmaluku());

                // Tarkistetaan voittiko jompikumpi
                if (!(peli.getTilanne() == Peli.Pelitilanne.PELI_JATKUU)) {
                    tarkistaVoittaja();
                }

            } // handle

        }); // btnHeitaNoppaa.setOnAction(new EventHandler<ActionEvent>()
        
        // Koneen vuoro -komentopainikkeen tapahtumankäsittelijä
        btnTietokoneenVuoro.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                
                // Jos peli ei jatku, se on päättynyt. Alustetaan uusi peli.
                if (!(peli.getTilanne() == Peli.Pelitilanne.PELI_JATKUU)) {
                    alustaPeli();
                }

                // Lopetetaan pelaajan vuoro ja alustetaan merkkijono, jonne
                // kerättiin pelaajan edellisen vuoron heitot
                peli.getPelaaja().lopetaVuoro();
                pelaajanVuoronHeitot = "";
                
                // Asetetaan pelaajan pistemäärä käyttöliittymään
                asetaPisteet(lblPelaajanPisteet, 
                        peli.getPelaaja().getPisteet());
                
                // Tietokoneen vuoro pelata
                peli.getTietokone().pelaa();
                
                // Esitetään viesti tietokoneen keräämistä pisteistä
                // käyttöliittymän alalaidassa
                naytaViesti("Tietokone heitti vuorollaan " + 
                        peli.getTietokone().getVuoronPisteet());
                
                // Asetetaan tietokoneen pistemäärä käyttöliittymään
                asetaPisteet(lblTietokoneenPisteet, 
                        peli.getTietokone().getPisteet());
                
                // Tarkistetaan voittiko jompikumpi
                if (!(peli.getTilanne() == Peli.Pelitilanne.PELI_JATKUU)) {
                    tarkistaVoittaja();
                }
                
                // Enabloidaan komentopainike, jolla heitetään noppaa, jotta
                // käyttäjä voi jatkaa pelaamista omalla vuorollaan
                btnHeitaNoppaa.setDisable(false);

            } // handle

        }); // btnTietokoneenVuoro.setOnAction(new EventHandler<ActionEvent>()
        
    } // start
    
    // Metodi, joka ottaa parametrina vastaab GridPane-luokan olion ja asettaa 
    // siihen sarakkeet
    private void luoSarakkeet(GridPane grid) {
        
        // Luodaan määritykset sarakkeille suhteellisilla leveyksillä
        ColumnConstraints sarake1 = new ColumnConstraints();
        sarake1.setHalignment(HPos.CENTER);
        sarake1.setPercentWidth(15);
        
        ColumnConstraints sarake2 = new ColumnConstraints();
        sarake2.setHalignment(HPos.CENTER);
        sarake2.setPercentWidth(25);
        
        ColumnConstraints sarake3 = new ColumnConstraints();
        sarake3.setHalignment(HPos.CENTER);
        sarake3.setPercentWidth(20);
        
        ColumnConstraints sarake4 = new ColumnConstraints();
        sarake4.setHalignment(HPos.CENTER);
        sarake4.setPercentWidth(25);
        
        ColumnConstraints sarake5 = new ColumnConstraints();
        sarake5.setHalignment(HPos.CENTER);
        sarake5.setPercentWidth(15);
        
        // Lisätään sarakkeiden määritykset ruudukkoon
        grid.getColumnConstraints().addAll(
                sarake1, sarake2, sarake3, sarake4, sarake5);
        
    } // luoSarakkeet
    
    // Metodi, joka ottaa parametrina vastaan GridPane-luokan olion ja 
    // asettaa siihen rivit
    private void luoRivit(GridPane grid) {
        
        // Luodaan määritykset riveille suhteellisilla korkeuksilla
        RowConstraints rivi1 = new RowConstraints();
        rivi1.setPercentHeight(80);
        
        RowConstraints rivi2 = new RowConstraints();
        rivi2.setPercentHeight(20);
        
        // Lisätään rivien määritykset ruudukkoon
        grid.getRowConstraints().addAll(rivi1, rivi2);
        
    } // luoRivit
    
    // Metodi, joka ottaa parametrina vastaan GridPane-luokan olion ja 
    // asettaa siihen käyttöliittymän ohjaimet
    private void luoOhjaimet(GridPane grid) {
        
        // Lisätään käyttöliittymäohjaimet ruudukkoon
        
        lblPelaajanPisteet = new Label();
        lblPelaajanPisteet.setId("omatPisteet");    // Tyylitiedoston id
        lblPelaajanPisteet.setTextFill(Color.web("#0080FF"));
        grid.add(lblPelaajanPisteet, 0, 0);     // Sarake 0, rivi 0
        
        btnHeitaNoppaa = new Button(HEITA_NOPPAA);
        grid.add(btnHeitaNoppaa, 1, 0);         // Sarake 1, rivi 0
        
        imgvKuvaOhjain = new ImageView();
        grid.add(imgvKuvaOhjain, 2, 0);         // Sarake 2, rivi 0
        
        String kuvatiedosto = KUVATIEDOSTOJEN_HAKEMISTO + 
                SIKA_KUVATIEDOSTO + KUVATIEDOSTOJEN_TARKENNE;
        imgKuva = new Image(getClass().getResourceAsStream(kuvatiedosto));
        imgvKuvaOhjain.setImage(imgKuva);
        
        btnTietokoneenVuoro = new Button(KONEEN_VUORO);
        grid.add(btnTietokoneenVuoro, 3, 0);    // Sarake 3, rivi 0
        
        lblTietokoneenPisteet = new Label();
        lblTietokoneenPisteet.setId("omatPisteet"); // Tyylitiedoston id
        grid.add(lblTietokoneenPisteet, 4, 0);  // Sarake 4, rivi 0
        
        // Asetetaan viesti käyttöliittymän alalaitaan siten, että se alkaa 
        // sarakkeesta 0 riviltä 1 vieden tilaa 5 saraketta ja 1 rivin
        lblViesti = new Label();
        grid.add(lblViesti, 0, 1, 5, 1);
        
        // Asetetaan tyylitiedoston id gridille
        grid.setId("grid");
        
    } // luoOhjaimet
    
    // Alustaa pelin
    private void alustaPeli() {
        noppa = new Noppa();
        pelaaja = new Pelaaja();
        peli = new Peli();
        tietokone = new Tietokone();
        lblPelaajanPisteet.setText("0");
        lblTietokoneenPisteet.setText("0");
        lblViesti.setText("Heitä noppaa tai anna vuoro koneelle...");
    }
    
    // Asettaa nopan kuvan käyttöliittymään parametrina välitetyn silmäluvun
    // mukaan
    private void asetaNopanKuva(int silmaluku) {
        String kuvatiedosto = KUVATIEDOSTOJEN_HAKEMISTO + silmaluku + 
                KUVATIEDOSTOJEN_TARKENNE;
        imgKuva = new Image(getClass().getResourceAsStream(kuvatiedosto));
        imgvKuvaOhjain.setImage(imgKuva);
    }
    
    // Näyttää pelaajan heitot vuorolla käyttöliittymän alalaidassa
    private void naytaPelaajanHeitot(int silmaluku) {
        
        if (pelaajanVuoronHeitot.isEmpty()) {
            lblViesti.setText(String.valueOf(silmaluku));
            pelaajanVuoronHeitot = String.valueOf(silmaluku);
        }
        else {
            pelaajanVuoronHeitot += ", " + silmaluku;
            lblViesti.setText(pelaajanVuoronHeitot);
        }
    }
    
    // Asettaa joko pelaajan tai tietokoneen pistemäärän käyttöliittymään 
    // parametrien perusteella
    private void asetaPisteet(Label lblPisteet, int pistemaara) {   
        lblPisteet.setText(String.valueOf(pistemaara));
    }
    
    // Näyttää parametrina välitetyn viestin käyttöliittymän alalaidassa
    private void naytaViesti(String viesti) {
        lblViesti.setText(viesti);
    }
    
    // Tarkistaa voittajan ja asettaa sen mukaan kuvan ja viestin
    private void tarkistaVoittaja() {
        
        if (peli.getTilanne() == Peli.Pelitilanne.PELAAJA_VOITTI) {
            asetaKuva(SMILEY_KUVATIEDOSTO);
            naytaViesti("Sinä voitit! Aloita uusi peli heittämällä noppaa...");
        }
        else {
            asetaKuva(SAD_SMILEY_KUVATIEDOSTO);
            naytaViesti("Tietokone voitti! Aloita uusi peli heittämällä "
                    + "noppaa...");
        }
    }
    
    // Asettaa kuvan käyttöliittymään parametrina välitetyn tiedoston nimen 
    // mukaan
    private void asetaKuva(String kuva) {
        String kuvatiedosto = KUVATIEDOSTOJEN_HAKEMISTO + kuva + 
                KUVATIEDOSTOJEN_TARKENNE;
        imgKuva = new Image(getClass().getResourceAsStream(kuvatiedosto));
        imgvKuvaOhjain.setImage(imgKuva);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}