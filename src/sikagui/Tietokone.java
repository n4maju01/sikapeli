/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sikagui;

import java.util.Random;

/**
 *
 * @author jjuntune
 */
public class Tietokone extends Pelaaja {       
    public void pelaa() {
        Random arpoja=new Random();
        int heittoja=0,silmaluku=0,maxPisteita=0,maxHeittoja=0;        
        
        maxPisteita=arpoja.nextInt(8)+13;
        maxHeittoja=arpoja.nextInt(4)+2;

        while (this.vuoronPisteet<maxPisteita || heittoja<maxHeittoja) {
            this.noppa.heita();
            silmaluku=this.noppa.getSilmaluku();
            if (silmaluku==1) {             
                this.vuoronPisteet=0;
                break;
            }
            this.vuoronPisteet+=silmaluku;
            heittoja++;
        }
        this.pisteet+=this.vuoronPisteet;                    
    }    
}
