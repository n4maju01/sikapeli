/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * http://noppapelit.com/sika-noppapeli.html
 */
package sikagui;

/**
 *
 * @author jjuntune
 */
public class Peli {    
    public enum Pelitilanne {
        PELAAJA_VOITTI,TIETOKONE_VOITTI, PELI_JATKUU
    }
    
    private Pelaaja pelaaja=null;
    private Tietokone tietokone=null;
    
    private final static int VOITTO_PISTEET=100;    
    
    public Peli() {
        this.pelaaja=new Pelaaja();
        this.tietokone=new Tietokone();
    }    
    
    public Pelaaja getPelaaja() {
        return this.pelaaja;
    }
    
    public Tietokone getTietokone() {
        return this.tietokone;
    }
    
    public Pelitilanne getTilanne() {
        if (this.pelaaja.getPisteet()>=VOITTO_PISTEET && 
                this.pelaaja.getPisteet() >= this.tietokone.getPisteet()) {
            return Pelitilanne.PELAAJA_VOITTI;
        }
        else if(this.tietokone.getPisteet()>=VOITTO_PISTEET) {
            return Pelitilanne.TIETOKONE_VOITTI;
        }
        else {
            return Pelitilanne.PELI_JATKUU;
        }
    }
}