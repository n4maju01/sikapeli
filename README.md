# Sikapeli #

### FI ###

Kouluprojekti. Yksinkertainen Javalla kirjoitettu noppapeli.

Säännöt löytyvät osoitteesta http://noppapelit.com/sika-noppapeli.html

### EN ###

A simple dice game written in Java as a school project.

The game is in Finnish and the rules can be found at http://noppapelit.com/sika-noppapeli.html